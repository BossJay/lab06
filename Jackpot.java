public class Jackpot
{
  public static void main(String[] args)
  {
    System.out.println("Welcome to Jackpot!");
    
    Board boardGame = new Board();
    boolean gameOver = false;
    int numOfTilesClosed = 0;
    
    while(gameOver == false)
    {
      System.out.println(boardGame);
      
      if (boardGame.playATurn() == true)
      {
        gameOver = true;
      }
      else
      {
        numOfTilesClosed += 1;
      } 
    }
    
    if (numOfTilesClosed >= 7)
    {
      System.out.println("Jackpot! You have won!");
    }
    else
    {
      System.out.println("Oops! You have lost. Better luck next time!");
    }
  }
}